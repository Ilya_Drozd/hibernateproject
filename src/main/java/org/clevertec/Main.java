package org.clevertec;

import org.clevertec.dao.impl.*;
import org.clevertec.dto.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.*;


public class Main {
    public static void main(String[] args) {
        //addEmployee("lastName1", "firstName1", new Date(), "+375441112121", "waiter", 1.1);
        //addEmployee("lastName2", "firstName2", new Date(), "+375442223232", "cook", 2.2);
        //addOrder(1, 1, 1, new Date());
        //addDishMenuIngredient();
        //addCookedDish(1, 2, 2, 1, new Date());
//        String empResult1 = getEmployee(2);
//        String empResult2 = getEmployee(3);
//        String orderResult = getOrder(1);
//        String cookedDishResult = getCookedDish(1);
//        System.out.println("\n" + empResult1 + "\n" + empResult2 + "\n" + orderResult + "\n" + cookedDishResult + "\n");
    }

    private static void addEmployee(String lastName, String firstName, Date birthDate, String phone, String post,
                                    Double salary) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            Employee employee = new Employee();
            employee.setLastName(lastName);
            employee.setFirstName(firstName);
            employee.setBirthDay(birthDate);
            employee.setPhone(phone);
            employee.setPost(post);
            employee.setSalary(salary);

            EmployeeDao employeeDao = new EmployeeDao(factory);
            employeeDao.add(employee);
        }
    }

    //    private static void addCookedDishAndEmployee() {
//        try(SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
//            Employee employee = new Employee();
//            employee.setLastName("lastName3");
//            employee.setFirstName("firstName3");
//            employee.setBirthDay(new Date());
//            employee.setPhone("+375447190463");
//            employee.setPost("post3");
//            employee.setSalary(3.3);
//
//            CookedDish cookedDish = new CookedDish();
//            cookedDish.setDishNumber(3);
//            cookedDish.setEmployee(employee);
//            cookedDish.setOrder("order3");
//            cookedDish.setDate(new Date());
//
//            Set<CookedDish> set = new HashSet<>();
//            set.add(cookedDish);
//            employee.setCookedDishes(set);
//
//            CookedDishDao dishDao = new CookedDishDao(factory);
//            dishDao.add(cookedDish);
//        }
//    }
    private static void addOrder(Integer orderNumber, Integer empId, Integer tableNumber, Date date) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            EmployeeDao employeeDao = new EmployeeDao(factory);
            OrderDao orderDao = new OrderDao(factory);

            Employee employee = employeeDao.findById(empId);

            Order order = new Order();
            order.setOrderNumber(orderNumber);
            order.setEmployee(employee);
            order.setTableNumber(tableNumber);
            order.setDate(date);
            orderDao.add(order);
        }
    }

    private static void addDishMenuIngredient(){
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            DishDao dishDao = new DishDao(factory);
            MenuDao menuDao = new MenuDao(factory);
            IngredientDao ingredientDao = new IngredientDao(factory);
            Dish dish = new Dish();
            dish.setDishTitle("title");
            dish.setCategory("category");
            dish.setCost(5.2);
            dish.setWeight(0.2);

            Menu menu = new Menu();
            menu.setTitle("menuTitle");

            Ingredient ingredient = new Ingredient();
            ingredient.setIngredientTitle("ingredientTitle");

            List<Menu> menus = new ArrayList<>();
            menus.add(menu);
            dish.setMenus(menus);

            List<Ingredient> ingredients = new ArrayList<>();
            ingredients.add(ingredient);
            dish.setIngredients(ingredients);

            List<Dish> dishes = new ArrayList<>();
            dishes.add(dish);
            menu.setDishes(dishes);
            ingredient.setDishes(dishes);

            menuDao.add(menu);
            ingredientDao.add(ingredient);
            dishDao.add(dish);
        }
    }

    private static void addCookedDish(Integer dishNumber,Integer dishId, Integer empId, Integer orderId, Date date) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            CookedDishDao cookedDishDao = new CookedDishDao(factory);
            OrderDao orderDao = new OrderDao(factory);
            EmployeeDao employeeDao = new EmployeeDao(factory);
            DishDao dishDao = new DishDao(factory);

            Employee employee = employeeDao.findById(empId);
            Order order = orderDao.findById(orderId);
            Dish dish = dishDao.findById(dishId);

            CookedDish cookedDish = new CookedDish();
            cookedDish.setDishNumber(dishNumber);
            cookedDish.setDish(dish);
            cookedDish.setEmployee(employee);
            cookedDish.setOrder(order);
            cookedDish.setDate(date);

            cookedDishDao.add(cookedDish);
        }
    }

    private static String getEmployee(Integer id) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            EmployeeDao employeeDao = new EmployeeDao(factory);
            return employeeDao.findById(id).toString();
        }
    }

    private static String getCookedDish(Integer id) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            CookedDishDao dishDao = new CookedDishDao(factory);
            return dishDao.findById(id).toString();
        }
    }

    private static String getOrder(Integer id) {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            OrderDao orderDao = new OrderDao(factory);
            return orderDao.findById(id).toString();
        }
    }

    private static void deleteCookedDishWithEmployee() {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            CookedDishDao dishDao = new CookedDishDao(factory);

            CookedDish cookedDish = dishDao.findById(23);
            dishDao.delete(cookedDish);
        }
    }

    private static void deleteCookedDish() {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            CookedDishDao dishDao = new CookedDishDao(factory);

            CookedDish cookedDish = new CookedDish();
            cookedDish.setId(25);
            cookedDish.setEmployee(null);
            dishDao.delete(cookedDish);
        }
    }

    private static void deleteEmployee() {
        try (SessionFactory factory = new Configuration().configure().buildSessionFactory()) {
            EmployeeDao employeeDao = new EmployeeDao(factory);

            Employee employee = employeeDao.findById(18);
            employeeDao.delete(employee);
        }
    }
}
