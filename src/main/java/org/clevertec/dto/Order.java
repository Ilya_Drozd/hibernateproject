package org.clevertec.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Data
@Entity
@Table(name = "ORDERS")
@NoArgsConstructor
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ORDER_NUMBER")
    private Integer orderNumber;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPLOYEE_ID")
    private Employee employee;


    @OneToMany(mappedBy = "order", fetch = FetchType.LAZY, cascade = CascadeType.ALL,  orphanRemoval = true)
    private Set<CookedDish> cookedDishSet;

//    @OneToMany(mappedBy = "dishOrder", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
//    @JoinColumn(name = "DISHES")
//    private List<Dish> dishList;

    @Column(name = "TABLE_NUMBER")
    private Integer tableNumber;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE")
    private Date date;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(orderNumber, order.orderNumber) &&
                Objects.equals(employee, order.employee) &&
                Objects.equals(cookedDishSet, order.cookedDishSet) &&
               // Objects.equals(dishList, order.dishList) &&
                Objects.equals(tableNumber, order.tableNumber) &&
                Objects.equals(date, order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, orderNumber, employee,
                //dishList,
                tableNumber, date);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderNumber=" + orderNumber +
                ", employee=" + employee +
               // ", dishList=" + dishList +
                ", tableNumber=" + tableNumber +
                ", date=" + date +
                '}';
    }
}
