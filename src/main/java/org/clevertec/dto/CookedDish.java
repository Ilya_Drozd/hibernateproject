package org.clevertec.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "COOKED_DISHES")
@NoArgsConstructor
public class CookedDish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "DISH_ID")
    private Dish dish;

    @Column(name = "DISH_NUMBER")
    private Integer dishNumber;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPLOYEE_ID")
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "ORDER_ID")
    private Order order;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE")
    private Date date;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CookedDish that = (CookedDish) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dishNumber, that.dishNumber) &&
                Objects.equals(employee, that.employee) &&
                Objects.equals(order, that.order) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dishNumber, employee, order, date);
    }
}
