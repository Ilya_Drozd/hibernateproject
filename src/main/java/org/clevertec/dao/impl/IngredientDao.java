package org.clevertec.dao.impl;

import org.clevertec.dao.Dao;
import org.clevertec.dto.Dish;
import org.clevertec.dto.Ingredient;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class IngredientDao implements Dao<Ingredient, Integer> {

    private final SessionFactory factory;

    public IngredientDao(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void add(Ingredient ingredient) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.save(ingredient);
            session.getTransaction().commit();
        }
    }

    @Override
    public Ingredient findById(Integer id) {
        try (Session session = factory.openSession()) {
            Ingredient ingredient = session.get(Ingredient.class, id);
//            if (ingredient != null) {
//                Hibernate.initialize(ingredient.getDishes());
//            }
            return ingredient;
        }
    }

    @Override
    public void update(Ingredient ingredient) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(ingredient);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Ingredient ingredient) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(ingredient);
            session.getTransaction().commit();
        }
    }
}
