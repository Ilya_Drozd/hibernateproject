package org.clevertec.dao.impl;

import org.clevertec.dao.Dao;
import org.clevertec.dto.Ingredient;
import org.clevertec.dto.Menu;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class MenuDao implements Dao<Menu, Integer> {

    private final SessionFactory factory;

    public MenuDao(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void add(Menu menu) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.save(menu);
            session.getTransaction().commit();
        }
    }

    @Override
    public Menu findById(Integer id) {
        try (Session session = factory.openSession()) {
            Menu menu = session.get(Menu.class, id);
//            if (menu != null) {
//                Hibernate.initialize(menu.getDishes());
//            }
            return menu;
        }
    }

    @Override
    public void update(Menu menu) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(menu);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Menu menu) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(menu);
            session.getTransaction().commit();
        }
    }
}
