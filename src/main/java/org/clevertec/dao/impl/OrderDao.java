package org.clevertec.dao.impl;

import org.clevertec.dao.Dao;
import org.clevertec.dto.CookedDish;
import org.clevertec.dto.Order;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class OrderDao implements Dao<Order, Integer> {

    private final SessionFactory factory;

    public OrderDao(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void add(Order order) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.save(order);
            session.getTransaction().commit();
        }
    }

    @Override
    public Order findById(Integer id) {
        try (Session session = factory.openSession()) {
            Order order = session.get(Order.class, id);
            if (order != null){
                Hibernate.initialize(order.getEmployee());
            }
            return order;
        }
    }

    @Override
    public void update(Order order) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(order);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Order order) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(order);
            session.getTransaction().commit();
        }
    }
}
