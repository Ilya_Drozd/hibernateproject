package org.clevertec.dao.impl;

import org.clevertec.dao.Dao;
import org.clevertec.dto.Dish;
import org.clevertec.dto.Order;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class DishDao implements Dao<Dish, Integer> {

    private final SessionFactory factory;

    public DishDao(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void add(Dish dish) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.save(dish);
            session.getTransaction().commit();
        }
    }

    @Override
    public Dish findById(Integer id) {
        try (Session session = factory.openSession()) {
            Dish dish = session.get(Dish.class, id);
            if (dish != null){
                Hibernate.initialize(dish.getIngredients());
                Hibernate.initialize(dish.getMenus());
            }
            return dish;
        }
    }

    @Override
    public void update(Dish dish) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.update(dish);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Dish dish) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.delete(dish);
            session.getTransaction().commit();
        }
    }
}
