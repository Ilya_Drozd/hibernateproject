package org.clevertec.dao.impl;

import org.clevertec.dao.Dao;
import org.clevertec.dto.Employee;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class EmployeeDao implements Dao<Employee, Integer> {

    private final SessionFactory factory;

    public EmployeeDao(SessionFactory factory) {
        this.factory = factory;
    }

    @Override
    public void add(Employee employee) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.save(employee);
            session.getTransaction().commit();
        }
    }

    @Override
    public Employee findById(Integer id) {
        try(Session session = factory.openSession()){
            Employee employee = session.get(Employee.class, id);
            return employee;
        }
    }

    @Override
    public void update(Employee employee) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.update(employee);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Employee entity) {
        try(Session session = factory.openSession()){
            session.beginTransaction();
            session.delete(entity);
            session.getTransaction().commit();
        }
    }
}
